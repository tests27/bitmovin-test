declare global {
  namespace NodeJS {
    interface ProcessEnv {
      BITMOVIN_API_KEY: string;
      BITMOVIN_ORGANIZATION_ID: string;
      S3_OUTPUT_BUCKET_NAME: string;
      S3_OUTPUT_ACCESS_KEY: string;
      S3_OUTPUT_SECRET_KEY: string;
    }
  }
}

export {};
