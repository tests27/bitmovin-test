import BitmovinApi, {
  AacChannelLayout,
  AclPermission,
  CodecConfigType,
  ConsoleLogger,
  DashManifest,
  DashManifestDefaultVersion,
  Encoding,
  EncodingMode,
  EncodingOutput,
  EncodingType,
  HlsManifestDefaultVersion,
  InputType,
  LiveDashManifest,
  LiveHlsManifest,
  ManifestType,
  MuxingType,
  Output,
  OutputType,
  ProfileH264,
  RtmpInput,
  S3Output,
  SrtInput,
  SrtMode,
  Thumbnail,
  ThumbnailUnit,
  Webhook,
  WebhookHttpMethod,
} from "@bitmovin/api-sdk";
import * as path from "path";
import {
  LiveEncodingData,
  LiveEncodingProtocol,
  S3_BUCKET,
  StopLiveEncodingData,
} from "./bitmovin.models";

const bitmovinApi = new BitmovinApi({
  apiKey: process.env.BITMOVIN_API_KEY,
  // uncomment the following line if you are working with a multi-tenant account
  tenantOrgId: process.env.BITMOVIN_ORGANIZATION_ID,
  logger: new ConsoleLogger(),
});

export async function startLiveEncoding(
  protocol: LiveEncodingProtocol,
  encodingData: LiveEncodingData
) {
  // Configs ====================
  const streamKey = encodingData.eventID;
  const outputPrefix = S3_BUCKET[encodingData.selectedRegion].prefixName;
  const encodingPath = "test2";
  // 'https://' +
  // process.env.API_WEBHOOKAPI_APIID +
  // '.execute-api.' +
  // process.env.REGION +
  // '.amazonaws.com/dev/public/webhook/bitmovin/' +
  // encodingData.eventID +
  // '/finish';
  const encodingName = `${
    protocol === "rtmp" ? "RtmpLiveEncoding" : "SrtLiveEncoding"
  }-${encodingData.eventID}`;

  // Encoding ====================
  const encoding = await bitmovinApi.encoding.encodings.create({
    type: EncodingType.LIVE,
    name: encodingName,
    description: encodingData.eventID,
    cloudRegion: encodingData.selectedRegion,
    customData: {
      inputName: encodingData.title,
    },
    // fallbackCloudRegions
    // encoderVersion
    // infrastructure
    // staticIpId
    // labels
  });

  // Webhooks ====================
  // const webhookUrl = "";
  // const encodingFinishedWebhook =
  //   bitmovinApi.notifications.webhooks.encoding.encodings.finished.createByEncodingId(
  //     encoding.id!,
  //     {
  //       url: webhookUrl,
  //       method: WebhookHttpMethod.POST,
  //       customData: {
  //         eventID: encodingData.eventID,
  //       },
  //     }
  //   );

  // Input ====================
  let input: RtmpInput | SrtInput | null = null;
  const inputName = encodingData.eventID;

  if (protocol === "rtmp") {
    const existingRtmpInputs = await bitmovinApi.encoding.inputs.rtmp.list();

    input = existingRtmpInputs.items![0];
  } else if (protocol === "srt") {
    const existingSrtInputs = await bitmovinApi.encoding.inputs.srt.list({
      name: inputName,
    });
    if (existingSrtInputs.items?.length) {
      input = existingSrtInputs.items[0];
    } else {
      input = await bitmovinApi.encoding.inputs.srt.create({
        name: inputName,
        mode: SrtMode.LISTENER,
        port: 2088,
        type: InputType.SRT,
        passphrase: streamKey,
        // host?: string;
        // path?: string;
        // latency?: number;
        // keyLength?: number;
        // backupSrtInputs?: BackupSrtInputs;
      });
    }
  }
  if (!input) {
    return null;
  }

  // Output
  const outputBucketName = S3_BUCKET[encodingData.selectedRegion].bucketName;
  const output = await getOrCreateS3OutputByBucketName(outputBucketName);

  // Video config ====================
  const videoRepresentation = encodingData.selectedVideoRepresentations[0];
  const videoConfigurationName = `H.264 ${videoRepresentation.height}p ${
    videoRepresentation.bitrate / (1000 * 1000)
  } Mbit/s`;

  const videoConfiguration =
    await bitmovinApi.encoding.configurations.video.h264.create({
      name: videoConfigurationName,
      type: CodecConfigType.H264,
      presetConfiguration: encodingData.presetConfiguration,
      height: videoRepresentation.height,
      width: Math.ceil(
        videoRepresentation.aspectRatio * videoRepresentation.height
      ),
      bitrate: videoRepresentation.bitrate,
      profile: ProfileH264.MAIN,
      encodingMode: encodingData.encodingMode,
    });

  // Audio config ====================
  const audioRepresentation = encodingData.selectedAudioRepresentations[0];
  const audioConfigurationName = `AAC ${
    audioRepresentation.bitrate / 1000
  } kbit/s`;

  const audioConfiguration =
    await bitmovinApi.encoding.configurations.audio.aac.create({
      type: CodecConfigType.AAC,
      name: audioConfigurationName,
      bitrate: audioRepresentation.bitrate,
      channelLayout: AacChannelLayout.CL_STEREO,
    });

  // Streams ====================
  const videoStream = await bitmovinApi.encoding.encodings.streams.create(
    encoding.id!,
    {
      codecConfigId: videoConfiguration.id,
      inputStreams: [
        {
          inputId: input.id,
          inputPath: "live",
          position: 0,
        },
      ],
    }
  );

  const audioStream = await bitmovinApi.encoding.encodings.streams.create(
    encoding.id!,
    {
      codecConfigId: audioConfiguration.id,
      inputStreams: [
        {
          inputId: input.id,
          inputPath: "live",
          position: 1,
        },
      ],
    }
  );

  // Muxings ====================
  const videoMuxing = await bitmovinApi.encoding.encodings.muxings.fmp4.create(
    encoding.id!,
    {
      type: MuxingType.FMP4,
      outputs: [
        buildEncodingOutput(
          output,
          outputPrefix,
          `${encodingPath}/video/${videoConfiguration.height}p`
        ),
      ],
      streams: [
        {
          streamId: videoStream.id,
        },
      ],
      writeDurationPerSample: true,
      segmentLength: encodingData.segmentLength,
    }
  );

  const audioMuxing = await bitmovinApi.encoding.encodings.muxings.fmp4.create(
    encoding.id!,
    {
      type: MuxingType.FMP4,
      outputs: [
        buildEncodingOutput(
          output,
          outputPrefix,
          `${encodingPath}/audio/${audioConfiguration.bitrate}kbps`
        ),
      ],
      streams: [
        {
          streamId: audioStream.id,
        },
      ],
      writeDurationPerSample: true,
      segmentLength: encodingData.segmentLength,
    }
  );

  // Manifests ====================
  const dashManifest = await bitmovinApi.encoding.manifests.dash.default.create(
    {
      encodingId: encoding.id,
      manifestName: "stream.mpd",
      version: DashManifestDefaultVersion.V1,
      outputs: [buildEncodingOutput(output, outputPrefix, encodingPath)],
    }
  );
  const vodDashManifest =
    await bitmovinApi.encoding.manifests.dash.default.create({
      name: "MPEG-DASH VoD Manifest",
      encodingId: encoding.id,
      manifestName: "vod.mpd",
      type: ManifestType.DASH,
      outputs: [buildEncodingOutput(output, outputPrefix, encodingPath)],
    });
  await bitmovinApi.encoding.manifests.dash.start(vodDashManifest.id!);

  const hlsManifest = await bitmovinApi.encoding.manifests.hls.default.create({
    encodingId: encoding.id,
    outputs: [buildEncodingOutput(output, outputPrefix, encodingPath)],
    name: "master.m3u8",
    version: HlsManifestDefaultVersion.V1,
  });

  const liveDashManifest = new LiveDashManifest({
    manifestId: dashManifest.id,
    timeshift: 1,
    liveEdgeOffset: encodingData.segmentLength,
  });
  const liveHlsManifest = new LiveHlsManifest({
    manifestId: hlsManifest.id,
    timeshift: 1,
    liveEdgeOffset: encodingData.segmentLength,
  });

  // Start encoding ====================
  await bitmovinApi.encoding.encodings.live.start(encoding.id!, {
    dashManifests: [liveDashManifest],
    hlsManifests: [liveHlsManifest],
    streamKey,
    // liveEncodingMode: encodingData.encodingMode,
    autoShutdownConfiguration: {
      bytesReadTimeoutSeconds: 2 * 60, // 10 minutes
      streamTimeoutMinutes: 5, // 5 minutes
    },
  });

  console.log("Live encoding started successfully");

  return {
    encodingID: encoding.id,
    outputDomain: "d3j1g55es9n34h.cloudfront.net",
    outputBucketName: outputBucketName,
    outputPrefix: outputPrefix,
    outputPath: `${encodingData.eventID}/live/${encoding.id}`,
    streamId: videoStream.id,
  };
}

export async function stopLiveEncoding(stopEncodingData: StopLiveEncodingData) {
  const outputBucketName = stopEncodingData.bucketName;
  const output = await getOrCreateS3OutputByBucketName(outputBucketName);

  const outputPrefix = stopEncodingData.outputPrefix;
  const encodingPath = stopEncodingData.encodingPath;

  const thumbnail = new Thumbnail({
    height: 480,
    width: 720,
    pattern: "thumbnail-%number%.png",
    positions: [2],
    unit: ThumbnailUnit.SECONDS,
    outputs: [buildEncodingOutput(output, outputPrefix, encodingPath)],
  });

  await bitmovinApi.encoding.encodings.streams.thumbnails.create(
    stopEncodingData.encodingId,
    stopEncodingData.videoStreamId,
    thumbnail
  );

  await bitmovinApi.encoding.encodings.live.stop(stopEncodingData.encodingId);

  return true;
}

function buildEncodingOutput(
  output: Output,
  outputPrefix: string,
  paths: string
): EncodingOutput {
  console.log("==============PATH", path.join(outputPrefix, paths));
  return new EncodingOutput({
    outputPath: path.join(outputPrefix, paths),
    outputId: output.id,
    acl: [
      {
        permission: AclPermission.PUBLIC_READ,
      },
    ],
  });
}

async function getOrCreateS3OutputByBucketName(
  bucketName: string
): Promise<S3Output> {
  let output: S3Output | null = null;

  const existingS3Outputs = await bitmovinApi.encoding.outputs.s3.list({
    name: bucketName,
  });

  if (existingS3Outputs.items?.length) {
    output = existingS3Outputs.items[0];
  } else {
    output = await bitmovinApi.encoding.outputs.s3.create({
      type: OutputType.S3,
      name: bucketName,
      bucketName: bucketName,
      accessKey: process.env.S3_OUTPUT_ACCESS_KEY,
      secretKey: process.env.S3_OUTPUT_SECRET_KEY,
    });
  }

  return output;
}
