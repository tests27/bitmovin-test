import {
  CloudRegion,
  EncodingMode,
  PresetConfiguration,
} from "@bitmovin/api-sdk";

export type LiveEncodingProtocol = "srt" | "rtmp";

export interface VideoRepresentation {
  type: string;
  bitrateLabel: string;
  resolutionLabel: string;
  bitrate: number;
  height: number;
  aspectRatio: number;
}

export interface AudioRepresentation {
  type: string;
  bitrateLabel: string;
  bitrate: number;
}

export const S3_BUCKET: {
  [key: string]: { bucketName: string; prefixName: string };
} = {
  [CloudRegion.AWS_EU_WEST_1]: {
    bucketName: "mimic-bitmovin-output",
    prefixName: "ireland",
  },
  [CloudRegion.AWS_US_WEST_1]: {
    bucketName: "mimic-bitmovin-output-california",
    prefixName: "california",
  },
  [CloudRegion.AWS_CA_CENTRAL_1]: {
    bucketName: "mimic-bitmovin-output-canadacentral",
    prefixName: "canadacentral",
  },
  [CloudRegion.AWS_EU_CENTRAL_1]: {
    bucketName: "mimic-bitmovin-output-frankfurt",
    prefixName: "frankfurt",
  },
  [CloudRegion.AWS_EU_WEST_2]: {
    bucketName: "mimic-bitmovin-output-london",
    prefixName: "london",
  },
  [CloudRegion.AWS_AP_SOUTH_1]: {
    bucketName: "mimic-bitmovin-output-mumbai",
    prefixName: "mumbai",
  },
  [CloudRegion.AWS_US_EAST_2]: {
    bucketName: "mimic-bitmovin-output-ohio",
    prefixName: "ohio",
  },
  [CloudRegion.AWS_US_WEST_2]: {
    bucketName: "mimic-bitmovin-output-oregon",
    prefixName: "oregon",
  }, //
  [CloudRegion.AWS_EU_WEST_3]: {
    bucketName: "mimic-bitmovin-output-paris",
    prefixName: "paris",
  },
  [CloudRegion.AWS_SA_EAST_1]: {
    bucketName: "mimic-bitmovin-output-saopaulo",
    prefixName: "saopaulo",
  },
  [CloudRegion.AWS_AP_NORTHEAST_2]: {
    bucketName: "mimic-bitmovin-output-seoul",
    prefixName: "seoul",
  },
  [CloudRegion.AWS_AP_SOUTHEAST_1]: {
    bucketName: "mimic-bitmovin-output-singapore",
    prefixName: "singapore",
  },
  [CloudRegion.AWS_EU_NORTH_1]: {
    bucketName: "mimic-bitmovin-output-stockholm",
    prefixName: "stockholm",
  },
  [CloudRegion.AWS_AP_SOUTHEAST_2]: {
    bucketName: "mimic-bitmovin-output-sydney",
    prefixName: "sydney",
  },
  [CloudRegion.AWS_AP_NORTHEAST_1]: {
    bucketName: "mimic-bitmovin-output-tokyo",
    prefixName: "tokyo",
  },
  [CloudRegion.AWS_US_EAST_1]: {
    bucketName: "mimic-bitmovin-output-virginia",
    prefixName: "virginia",
  },
};

export interface LiveEncodingData {
  eventID: string;
  title: string;
  framesPerCmafChunk?: number;
  selectedVideoRepresentations: VideoRepresentation[];
  selectedAudioRepresentations: AudioRepresentation[];
  selectedRegion: CloudRegion;
  segmentLength: number;
  encodingMode: EncodingMode;
  presetConfiguration: PresetConfiguration;
}

export interface StopLiveEncodingData {
  encodingId: string;
  bucketName: string;
  outputPrefix: string;
  encodingPath: string;
  videoStreamId: string;
}
