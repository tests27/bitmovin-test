import {
  CloudRegion,
  EncodingMode,
  PresetConfiguration,
} from "@bitmovin/api-sdk";
import { config as configDotenv } from "dotenv";
import * as path from "path";
import express from "express";

configDotenv({
  path: path.resolve(__dirname, "..", ".env"),
});

import { startLiveEncoding, stopLiveEncoding } from "./live";
import { StopLiveEncodingData } from "./bitmovin.models";

const app = express();
app.use(express.json());

const encodingType: "srt" | "rtmp" = "rtmp";
const encodingData = {
  eventID: "any-event-id",
  title: "qwe",
  framesPerCmafChunk: 100,
  selectedVideoRepresentations: [
    {
      aspectRatio: 1.7777777777777777,
      bitrate: 240000,
      bitrateLabel: "240 kbit",
      height: 216,
      resolutionLabel: "384 x 216",
      type: "H264",
    },
  ],
  selectedAudioRepresentations: [
    { type: "AAC", bitrateLabel: "128 kbit", bitrate: 128000 },
  ],
  selectedRegion: CloudRegion.AWS_EU_WEST_1,
  segmentLength: 10,
  encodingMode: EncodingMode.SINGLE_PASS,
  presetConfiguration: PresetConfiguration.LIVE_VERYLOW_LATENCY,
};

app.post("/start", async (req, res) => {
  const liveEncodingInfo = await startLiveEncoding(encodingType, encodingData);

  return res.status(201).json(liveEncodingInfo);
});

app.post("/stop", async (req, res) => {
  const stopEncodingData: StopLiveEncodingData = req.body;

  const stopRes = await stopLiveEncoding(stopEncodingData);

  return res.status(200);
});

const PORT = process.env.PORT || 4000;
app.listen(PORT, () => console.log(`Listening http://localhost:${PORT}`));
